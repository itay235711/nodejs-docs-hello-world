/**
 * Created by itay on 1/7/2017.
 */

const YoutubeApi = require("youtube-node");
const youtube = new YoutubeApi();
const opener = require('opener');
const cp = require('child_process');

(function () {
    // opener('https://docs.npmjs.com/cli/uninstall');

    // var urls = ['https://www.npmjs.com/package/opener',
    //     'https://docs.npmjs.com/cli/uninstall',
    //     'https://docs.npmjs.com/cli/uninstall'];
    //
    // openUrlsInNewChromeWindow(urls);

    // https://www.youtube.com/watch?v=HcwTxRuq-uk
    // https://www.youtube.com/v/HcwTxRuq-uk

    youtube.setKey('AIzaSyB1OOSpTREs85WUMvIgJvLTZKye4BVsoFU');

    youtube.search('World War z Trailer', 2, function(error, result) {
        if (error) {
            console.log(error);
        }
        else {
            console.log(JSON.stringify(result, null, 2));
        }
    });
})();

function openUrlsInNewChromeWindow(urls) {
    var firstUrl = urls.splice(0, 1)[0];

    cp.execFileSync('cmd', ['/c', 'start', 'chrome', '/new-window', firstUrl]);
    setTimeout(function () {

        for (var i = 0; i < urls.length; i++) {
            var url = urls[i];
            cp.execFileSync('cmd', ['/c', 'start', 'chrome', url]);
        }
    }, 500);
}
